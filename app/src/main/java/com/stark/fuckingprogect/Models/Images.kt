package com.stark.fuckingprogect.Models

import com.google.gson.annotations.SerializedName

class Images(
    val name: String,
    val description: String,
    val mission: String,
    val collection: String,
    @SerializedName("image_files")
    val image: List<UrlImages>
)

class UrlImages(
    @SerializedName("file_url")
    val fileUrl: String,
    @SerializedName("file_size")
    val fileSize: Int,
    val width: Int,
    val height: Int
)