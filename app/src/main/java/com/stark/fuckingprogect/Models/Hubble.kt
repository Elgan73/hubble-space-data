package com.stark.fuckingprogect.Models

import com.google.gson.annotations.SerializedName

class Hubble(
    @SerializedName("")
    val news : Post
)

class Post(
    val id: Int,
    val name : String,
    @SerializedName("news_name")
    val newsName : String,
    val collection: String
)