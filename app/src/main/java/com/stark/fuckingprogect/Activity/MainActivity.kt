package com.stark.fuckingprogect.Activity


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.stark.fuckingprogect.Adapters.StarsAdapter
import com.stark.fuckingprogect.Models.Post
import com.stark.fuckingprogect.R
import kotlinx.android.synthetic.main.activity_posts.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)
        fetchJsonNews()
        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    fun fetchJsonNews() {
        val url = "https://hubblesite.org/api/v3/images"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                val gson = GsonBuilder().create()
                val newPost = gson.fromJson<List<Post>>(body, object : TypeToken<List<Post>>() {}.type)
                runOnUiThread {
                    recyclerView.adapter = StarsAdapter(newPost, {starsItem: Post -> starsItemClicked(starsItem)})
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Toast.makeText(this@MainActivity, "Error: $e", Toast.LENGTH_SHORT).show()
            }
        })
    }


    fun starsItemClicked(starsList: Post) {
        val intent = Intent(this, StarsActivity::class.java)
        intent.putExtra("itemId", starsList.id.toString())
        startActivity(intent)
    }

}



