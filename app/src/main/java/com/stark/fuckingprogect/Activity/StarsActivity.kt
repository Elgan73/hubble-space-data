package com.stark.fuckingprogect.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import com.stark.fuckingprogect.Models.Images
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class StarsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.stark.fuckingprogect.R.layout.activity_main)
        val intentStars = getIntent()

        if (intentStars.hasExtra("itemId")) {
            var itemStarsId = intentStars.getStringExtra("itemId")
            imagesHubbleJson(itemStarsId)
        }
    }

    fun imagesHubbleJson(id: String) {
        val url = "https://hubblesite.org/api/v3/image/$id"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                val gson = GsonBuilder().create()
                val imagesString = gson.fromJson(body, Images::class.java)
                val starName = imagesString.name
                val starMission = imagesString.mission
                val starCollection = imagesString.collection
                val starDescription = imagesString.description
                val starImage = imagesString.image[0].fileUrl
                val nameSite = "hubblesite.org"
                val substringData = starImage.substringAfter("hvi")
                val imageData = "https://" + nameSite + substringData
                runOnUiThread {
                    Picasso.get().load(imageData).into(starImages)
                    nameStar.setText(starName)
                    missionStar.setText("Mission: " + starMission.capitalize())
                    collectionStar.setText("Collection: " + starCollection.capitalize())
                    descriptionStar.setText(starDescription)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Toast.makeText(this@StarsActivity, "Failure response JSON", Toast.LENGTH_SHORT).show()
            }
        })
    }
}


