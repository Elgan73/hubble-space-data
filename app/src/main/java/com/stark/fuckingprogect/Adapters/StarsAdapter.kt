package com.stark.fuckingprogect.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stark.fuckingprogect.Models.Hubble
import com.stark.fuckingprogect.Models.Post
import com.stark.fuckingprogect.R
import kotlinx.android.synthetic.main.row_main.view.*

class StarsAdapter(val starsItem : List<Post>, val clickListener: (Post) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row_main, parent, false)
        return StarsViewHolder(view)

    }

    override fun getItemCount(): Int {
        return starsItem.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.itemId.text = starsItem[position].id.toString()
        holder.itemView.itemName.text = starsItem[position].name
        holder.itemView.itemCollection.text = starsItem[position].collection
        holder.itemView.setOnClickListener { clickListener(starsItem[position]) }
    }

    class StarsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}